export default [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi libero quos, laborum quod dolorum magnam cum saepe fugiat nam animi, provident cumque in id. Repudiandae exercitationem sequi quaerat blanditiis consequuntur?",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi libero quos, laborum quod dolorum magnam cum saepe fugiat nam animi, provident cumque in id. Repudiandae exercitationem sequi quaerat blanditiis consequuntur?",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi libero quos, laborum quod dolorum magnam cum saepe fugiat nam animi, provident cumque in id. Repudiandae exercitationem sequi quaerat blanditiis consequuntur?",
        price: 55000,
        onOffer: true
    }
]
