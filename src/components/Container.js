import React from "react";
import { Link } from "react-router-dom";

export default function Container() {
    return (

        <div className="container">
            <h1>Error 404</h1>
            <h1>Site Not Found</h1>

            <Link to="/">Back to Home</Link>
        </div>
        
    )
}