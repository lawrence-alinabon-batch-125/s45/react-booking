import React, { Fragment, useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';

import UserContext from './../UserContext';


export default function AppNavbar() {
    // console.log(props)
  
  // let user = props.user
  // destructure context object
  const { user, unsetUser } = useContext(UserContext)
  // useHistory is a react-router-dom
  let history = useHistory();

  const logout = () => {
    unsetUser();
    history.push('/login')
  }

  let leftNav = (user.id !== null) ? (
    (user.isAdmin === true) ?
      (
        <Fragment >
          <Nav.Link as={NavLink} to="/courses">Add Course</Nav.Link>
          <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
      )
      :
      (
        <Nav.Link onClick={logout}>Logout</Nav.Link>
      )
    )
    :
    (
      <Fragment >
        <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
        <Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
    </Fragment>
    )
      


  return (
    <Navbar bg="info" expand="lg">
    <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav>
        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
        {/* <Nav.Link as={NavLink} to="/courses" >Courses</Nav.Link> */}
      </Nav>
      <Nav>
        {leftNav}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
  )
}