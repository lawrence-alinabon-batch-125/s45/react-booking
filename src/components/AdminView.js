import React, {useEffect, useState} from 'react';
import {Container, Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props) {
    // console.log(props)

    const { courseData, fetchData } = props;
    const [courseId, setCourseId] = useState('');
    const [courses, setCourses] = useState([]);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [showEdit, setShowEdit] = useState(false)
    const [showAdd, setShowAdd] = useState(false)
    
    
    let token = localStorage.getItem('token')

    const openAdd = () => setShowAdd(true);

    const closeAdd = () => setShowAdd(false);

    const openEdit = (courseId) => {
		
		fetch(`https://stormy-woodland-60735.herokuapp.com/api/courses/${courseId}`,{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)

			setCourseId(result._id);
			setName(result.name);
			setDescription(result.description);
			setPrice(result.price)
        })
        
        setShowEdit(true)


    }
    
    const closeEdit = () => {

        setShowEdit(false);
        setName('');
        setDescription('');
        setPrice(0);
    }
    
	useEffect( () => {
		const coursesArr = courseData.map( (course) => {
			console.log(course)
			return(
				<tr key={course._id}>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td>
						{
							(course.isActive === true) ?
								<span>Available</span>
							:
								<span>Unavailable</span>
						}
					</td>
					<td>
						<Button variant="primary" size="sm" 
						onClick={ ()=> openEdit(course._id) }>
							Update
                        </Button>
                        
                        {
                            (course.isActive === true) ?
                                <Button variant="danger" size="sm"
                                onClick={()=> archiveToggle(course._id, course.isActive)}>
                                    Disable
                                </Button>
                            :
                                <Button variant="success" size="sm"
                                onClick={ () => archiveToggle(course._id, course.isActive)}>
                                    Enable
                                </Button>
                        }
                        
					</td>
				</tr>
			)
		})

		setCourses(coursesArr)
	}, [courseData])

    const editCourse = (e) => {
        e.preventDefault()

        fetch(`https://stormy-woodland-60735.herokuapp.com/api/courses/${courseId}/edit`, {
            method: 'PUT',
            headers: {
                'Content-Type': "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(result => result.json())
            .then(result => {
            
            fetchData()
            if (typeof result !== "undefined") {
                // alert('success')
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Course Successfully Updated'

                })
                
                closeEdit();
            } else {
                fetchData()
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Course Not Updated'

                })
                closeEdit();
            }
        })
    }

    const archiveToggle = (courseId, isActive) => {
        fetch(`https://stormy-woodland-60735.herokuapp.com/api/courses/${courseId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                isActive: isActive
            })
        })
        .then(result => result.json())
        .then(result => {
                fetchData();
            if (result === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Course Successfully Archived'

                })
            } else {
                fetchData();
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Course Not Archived'

                })
            }
        })
    }

    const addCourse = (e) => {
        e.preventDefault();

        fetch(`https://stormy-woodland-60735.herokuapp.com/api/courses/addCourse`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if (result === true) {
                fetchData()
                Swal.fire({
                    title: 'Success',
                    icon: 'icon',
                    text: 'Course Successfully Added'

                })
                setName('')
                setDescription('')
                setPrice(0)
                closeAdd()
            } else {
                fetchData();
                Swal.fire({
                    title: 'Error',
                    icon: 'icon',
                    text: 'Course Not Added'

                })
                closeAdd()
            }
        })
    }

    return (
        <Container>
            <div>
                <h2 className="text-center">Admin Dashboard</h2>
                <div className="d-flex justify-content-end mb-2"> 
                    <Button variant='primary' onClick={openAdd}>Add New Course</Button>
                </div>
            </div>
            <Table>
                <thead>
                    <td>Name</td>
                    <td>Description</td>
                    <td>Price</td>
                    <td>Availability</td>
                     <td>Action</td>
                </thead>
                <tbody>
                    {/*display the courses*/}
					{courses}

                </tbody>
            </Table>

            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={ (e) => editCourse(e, courseId)}>
                    <Modal.Header>
                        <Modal.Title>Edit A Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId='courseName'>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type='text'
                                value={name}
                                onChange={ (e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='courseDescription'>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type='text'
                                value={description}
                                onChange={ (e) => setDescription(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='coursePrice'>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type='number'
                                value={price}
                                onChange={ (e) => setPrice(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            {/* Add Course Modal */}
            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={(e) => addCourse(e)}>
                    <Modal.Header>
                        <Modal.Title>Add Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId='courseName'>
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type='text'
                                value={name}
                                onChange={ (e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='courseDescription'>
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type='text'
                                value={description}
                                onChange={ (e) => setDescription(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId='coursePrice'>
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type='number'
                                value={price}
                                onChange={ (e) => setPrice(e.target.value)} />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type='submit'>Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </Container>
    )
}
