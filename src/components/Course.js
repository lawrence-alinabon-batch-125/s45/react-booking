import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { PropTypes } from 'prop-types'
import { Link } from "react-router-dom";

export default function Course({courseProp}) {
    // console.log(props)
    // let course = props.course

    const {name, description, price, _id} = courseProp

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);
    // const [isDisabled, setIsDisabled] = useState(false)

    // const enroll = () => {
    //     if (count < 30) {
    //         setCount(count + 1);

    //         setSeats(seats - 1);
    //     }
    //     // else {
    //     //     alert('Slots not Available');
    //     // }    
    // }
    // useEffect(() => {
    //         if (seats === 0) {
    //             setIsDisabled(true)
    //         }
    // }, [count])
    
    return (
        <Card>
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                    <h6>Description:</h6>
                    <p> {description} </p>
                    <h6>Price:</h6>
                    <p>PhP {price} </p>
                    {/* <h6>Enrollees</h6>
                    <p>{count} Enrollees</p>
                    <p>{seats} Seats Available</p> */}
                {/* <Button variant="primary" onClick={enroll} 
                disabled = {isDisabled}>Enroll</Button> */}

                    <Link className='btn btn-primary'to={`/courses/${_id}`} >Details</Link>

            </Card.Body>
        </Card>

    )
}

Course.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}