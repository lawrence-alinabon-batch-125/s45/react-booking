import React from "react";
import { Col, Row, Card, Button } from "react-bootstrap";


export default function CourseCard() {
    return (
        <Row className ="px-3 py-2">
            <Col xs={12}>
                <Card>
                    <Card.Body>
                        <Card.Title>Sample Course</Card.Title>
                            <h6>Description:</h6>
                            <p>This is a sample course offering</p>
                            <h6>Price:</h6>
                            <p>PhP 40,000</p>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}