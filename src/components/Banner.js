import React from 'react';
import { Button, Col, Row, Jumbotron} from 'react-bootstrap';

export default function Banner() {
    return (
        // <div className="row">
        //     <div className="col-10 col-md-8">
        //         <div className="jumbotron">
        //             <h1>Zuitt Bootcamp</h1>
        //             <p>Opportunities for everyone, everywhere</p>
        //             <button className="btn btn-primary">Enroll</button>
        //         </div>
        //     </div>
        // </div>

        <Row>
            <Col className="px-0">
                <Jumbotron fluid className="px-3">
                    <h1>Zuitt Bootcamp</h1>
                    <p>Opportunities for everyone, everywhere</p>
                    <Button className="btn btn-primary">Enroll</Button>
                </Jumbotron>
            </Col>
        </Row>

)
}