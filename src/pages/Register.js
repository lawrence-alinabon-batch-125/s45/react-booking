import React, { useState, useEffect, useContext} from "react";
import { Container, Form, Button } from "react-bootstrap";
import { Redirect, useHistory } from "react-router";
import UserContext from "./../UserContext";
import Swal from 'sweetalert2';

export default function Register() {

    const [firstName, setfirstName] = useState('');
    const [lastName, setlastName] = useState('');
    const [mobileNo, setmobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);

    const {user} = useContext(UserContext);

    useEffect(() => {
        if (email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        } 
    }, [email, password, verifyPassword])

    let history = useHistory();

    function register(e) {
        e.preventDefault();

        // alert('Registration Successful, You may now log in');
        fetch('https://stormy-woodland-60735.herokuapp.com/api/users/checkEmail', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(result => result.json())
        .then(result => {
            // console.log(result)

            if (result === true) {
                Swal.fire({
                    title: 'Duplicate email Found',
                    icon:'error',
                    text: 'Please choose another email',
                    button: 'OK'
                })
            } else {
                fetch('https://stormy-woodland-60735.herokuapp.com/api/users/register', {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        mobileNo: mobileNo,
                        email: email,
                        password : password
                    })
                }).then(result => result.json())
                .then(result => {
                    console.log(result)

                    Swal.fire({
                        title: 'Duplicate email Found',
                        icon:'success',
                        text: 'Please choose another email',
                        button: 'OK'
                    })
                    history.push('/login')

                })
            }
            
        })

        
        setEmail('')
        setPassword('')
        setVerifyPassword('')
    
    }

    return (
        (user.id !== null) ?
            <Redirect to='/' />
        :
            <Container className="mt-5">
                <h1>Register</h1>
                <Form onSubmit={(e) => register(e) }>
                    <Form.Group className="mb-3" controlId="formfirstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter First Name" value={ firstName } onChange={(e) => setfirstName(e.target.value) }/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formlastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Last Name" value={ lastName } onChange={(e) => setlastName(e.target.value) }/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formmobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control type="text" placeholder="Enter Mobile Number" value={ mobileNo } onChange={(e) => setmobileNo(e.target.value) }/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={ email } onChange={(e) => setEmail(e.target.value) }/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={ password} onChange={ (e) => setPassword(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formVerifyPassword">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={ (e) => setVerifyPassword(e.target.value)}/>
                    </Form.Group>

                    <Button variant="primary" type="submit" disabled={isDisabled}>
                        Submit
                    </Button>
                    </Form>
            </Container>
    )
}
