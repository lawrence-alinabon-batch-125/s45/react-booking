import React, { useContext, useEffect, useState } from "react";
import { Button, Card, Container } from "react-bootstrap";
import { Link, useParams, useHistory } from "react-router-dom";
import UserContext from "./../UserContext";
import Swal from 'sweetalert2'


export default function SpecificCourse() {
    
    const {user} = useContext(UserContext)
    const { courseId } = useParams();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    let token = localStorage.getItem('token')
    let history = useHistory();
    useEffect(() => {
        fetch(`https://stormy-woodland-60735.herokuapp.com/api/courses/${courseId}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            
            setName(result.name);
            setDescription(result.description)
            setPrice(result.price)
        })    
    }, [])

    const enroll = () => {
        fetch(`https://stormy-woodland-60735.herokuapp.com/api/users/enroll`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
            .then(result => result.json())
            .then(result => {
            if (result === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Course Successfully Enrolled'

                });
                history.push('/courses')
            } else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Course Not Enrolled'

                })
                
            }
        })
    }
    return (
        <Container>
            <Card>
                <Card.Header>
                    <h4>
                        {name}
                    </h4>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <h6>
                        Price: PhP {price}
                    </h6>
                </Card.Body>
                <Card.Footer>
                    {
                        (user.id !== null) ?
                            <Button variant="primary" onClick={ () => enroll()}>Enroll</Button>
                        :
                            <Link className='btn btn-primary' to='/login'> Log in to enroll </Link>    
                    }
                </Card.Footer>
            </Card>
        </Container>
    )
}