import React, {useContext, useEffect, useState} from "react";
import { Container } from "react-bootstrap";
//components
import Course from './../components/Course';
// mock data
// import courses from './../mock-data/courses';

import AdminView from "./../components/AdminView";
import UserContext from "./../UserContext";
import UserView from "./../components/UserView"; 

export default function Courses() {
    
    let token = localStorage.getItem("token");
    const [courses, setCourses] = useState([]);

    const {user} = useContext(UserContext)

    const fetchData = () => {
        fetch('https://stormy-woodland-60735.herokuapp.com/api/courses/all', {
            method: "GET" ,
            header: {
                "Authorization" : `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setCourses(result)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])
    
    // let CourseCards = courses.map((course) => {
    //     return <Course key={course.id} course={course}/>
    // })
    
    return (
        <Container fluid>
        {(user.isAdmin === true) ?
            <AdminView courseData={courses} fetchData={fetchData}/>
            :
            <UserView courseData={courses}/>   } 
        </Container>
    )
}